﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;

namespace final_project
{
    class Manager
    {
   
        private int RoadNum;
        private Car car;
        private DispatcherTimer EnemyTimer;
        private List<BaseClass> EnemyCars;
        private Canvas arena;
        private DispatcherTimer CrashTimer;
        private DispatcherTimer LostTimer;
       
        public int lives;
        public int money;
       
        public event EventHandler RemoveLife;
        public event EventHandler pointsAmount;
        public Manager(Canvas arena)
        {
            this.arena = arena;
            this.car = new Car( arena, 650, 450,100,100 );
            this.RoadNum = 1;
            this.lives = 3;
            this.money = 0;
           
            this.CrashTimer = new DispatcherTimer();
            this.CrashTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            this.CrashTimer.Start();
            this.CrashTimer.Tick += CrashTimer_Tick;
            this.EnemyCars = new List<BaseClass>();
            this.EnemyTimer = new DispatcherTimer();
            this.EnemyTimer.Interval = new TimeSpan(0, 0, 0, 0,650);
            this.EnemyTimer.Start();
            this.EnemyTimer.Tick += EnemyTimer_Tick;
            this.LostTimer = new DispatcherTimer();
            this.LostTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);


            this.LostTimer.Start();
         
        }

        

        private void Lost()
        {
            var dialog = new MessageDialog("Your lost!").ShowAsync();
            this.EnemyTimer.Stop();
            this.CrashTimer.Stop();
            
           
           
        }

        private void CrashTimer_Tick(object sender, object e)
        {

            Rect current_car = this.car.GetRectangle();
            for (int i = 0; i < this.EnemyCars.Count; i++)
            {
                if (IsBounceOtherObject(current_car, this.EnemyCars[i].GetRectangle()))
                {
                    if (this.EnemyCars[i].GetType() == typeof(Enemy)) // crach into an enemy car
                    {
                        this.lives = this.lives - 1;
                        RemoveLife(lives, null);
                        this.arena.Children.Remove(this.EnemyCars[i].GetImage());
                        this.EnemyCars.RemoveAt(i);
                        if(this.lives==0)
                        {
                            Lost();
                        }
                    }
                    else if (this.EnemyCars[i].GetType() == typeof(Coin)) // crash into coin
                    {
                        this.money++;
                        this.arena.Children.Remove(this.EnemyCars[i].GetImage());
                        this.EnemyCars.RemoveAt(i);
                    }
                }
            }

        }

        public bool IsBounceOtherObject(Rect a_car, Rect b_car)
        {
            return (Math.Abs(a_car.X - b_car.X) * 2 < (a_car.Width + b_car.Width) &&
                Math.Abs(a_car.Y - b_car.Y) * 2 < (a_car.Height+ b_car.Height));
        }

        private void EnemyTimer_Tick(object sender, object e)
        {
            Random rnd = new Random();
            Random rnd1 = new Random();
            int num = rnd.Next(0, 6);
            int color = rnd1.Next(0,3); 

            switch (num)
            {
                case 0:
                    this.EnemyCars.Add(new Enemy(arena, 400, 0, 100, 100, color));
                    break;
                case 1:
                    this.EnemyCars.Add(new Enemy(arena, 650, 0, 100, 100, color));
                    break;
                case 2:
                    this.EnemyCars.Add(new Enemy(arena, 850, 0, 100, 100, color));
                    break;
                case 3:
                    this.EnemyCars.Add(new Coin(arena, 400, 0, 100, 100));
                    break;
                case 4:
                    this.EnemyCars.Add(new Coin(arena, 650, 0, 100, 100));
                    break;
                case 5:
                    this.EnemyCars.Add(new Coin(arena, 850, 0, 100, 100));
                    break;

            }
            
            
            for (int i = 0; i < this.EnemyCars.Count; i++)
            {
                if (this.EnemyCars[i].GetY() > 1000)
                {
                    this.arena.Children.Remove(this.EnemyCars[i].GetImage());
                    this.EnemyCars.RemoveAt(i);
                }
            }
        }

        internal void Move(VirtualKey virtualKey)
        {
            if (virtualKey == VirtualKey.Right&& RoadNum<2)
            {
                this.RoadNum++;
                this.car.SetX(this.car.GetX()+250);
            }
            if (virtualKey == VirtualKey.Left && RoadNum > 0)
            {
                this.RoadNum--;
                this.car.SetX(this.car.GetX() - 250);
            }

        }
    }
}
