﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace final_project
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Gamepage : Page
    {
        private Manager manager;
        private int Score = 0;
        private DispatcherTimer pointstimer;

        public Gamepage()
        {
            this.InitializeComponent();
           
            this.pointstimer = new DispatcherTimer();
            this.pointstimer.Interval = new TimeSpan(0, 0, 1);
            this.pointstimer.Start();
            this.pointstimer.Tick += Pointstimer_Tick;

        }

        private void Pointstimer_Tick(object sender, object e)
        {

            this.Score++;
            Scoretext.Text = "Score:" + Score.ToString();
            moneytext.Text = "coins:" + manager.money.ToString();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.manager = new Manager(arena);
            Window.Current.CoreWindow.KeyDown += CoreWindow_KeyDown;

            this.manager.RemoveLife += Manager_RemoveLife;
           
        }

       

        private void Manager_RemoveLife(object sender, EventArgs e)
        {
            int life = (int)sender;
            if(life==2)
            {
                topGrid.Children.Remove(heart3);
            }
            else if (life == 1)
            {
                topGrid.Children.Remove(heart2);

            }
            else if (life == 0)
            {
                topGrid.Children.Remove(heart1);
                this.pointstimer.Stop();
               
            }
        }

        private void CoreWindow_KeyDown(CoreWindow sender, KeyEventArgs args)
        {
            this.manager.Move(args.VirtualKey);
        }
       
    }
}
