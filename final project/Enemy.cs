﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace final_project
{
    class Enemy : BaseClass
    { 
        protected float speedY;
        protected DispatcherTimer moveTimer;
        public Enemy(Canvas arena, float x, float y, int width, int height, int color)
            : base(arena, x, y, width, height)
        {
            
            GoDown();
            this.moveTimer = new DispatcherTimer();
            this.moveTimer.Start();
            this.moveTimer.Interval = TimeSpan.FromMilliseconds(1);
            this.moveTimer.Tick += MoveTimer_Tick;
            switch (color)
            {
                case 0:
                    this.image.Source = new BitmapImage(new Uri("ms-appx:///Assets/gray.png"));
                    break;
                case 1:
                    this.image.Source = new BitmapImage(new Uri("ms-appx:///Assets/red.png"));
                    break;
                case 2:
                    this.image.Source = new BitmapImage(new Uri("ms-appx:///Assets/orange.png"));
                    break;
            }
        }
        internal void GoDown()
        {
            this.speedY = 10;
        }
        private void MoveTimer_Tick(object sender, object e)
        {
            this.y += this.speedY;
            Canvas.SetLeft(this.image, x);
            Canvas.SetTop(this.image, y);
        }
    }
}