﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace final_project
{
    class Coin : BaseClass
    {
        protected float speedY;
        protected DispatcherTimer moveTimer;
        public Coin(Canvas arena, float x, float y, int width, int height)
            : base(arena, x, y, width, height)
        {
            this.image.Source = new BitmapImage(new Uri("ms-appx:///Assets/coin.png"));
            GoDown();
            this.moveTimer = new DispatcherTimer();
            this.moveTimer.Start();
            this.moveTimer.Interval = TimeSpan.FromMilliseconds(1);
            this.moveTimer.Tick += MoveTimer_Tick;
        }
        internal void GoDown()
        {
            this.speedY = 10;
        }
        private void MoveTimer_Tick(object sender, object e)
        {
            this.y += this.speedY;
            Canvas.SetLeft(this.image, x);
            Canvas.SetTop(this.image, y);
        }
    }
}
