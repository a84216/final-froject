﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;

namespace final_project
{
    class BaseClass
    {
        protected float x;
        protected float y;
        protected Image image;
        protected Canvas arena;
        public BaseClass(Canvas arena, float x, float y, int width, int height)
        {
            this.arena = arena;
            this.x = x;
            this.y = y;
            this.image = new Image();
            this.image.Width = width;
            this.image.Height = height;
            Canvas.SetLeft(this.image, x);
            Canvas.SetTop(this.image, y);
            this.arena.Children.Add(this.image);
        }
        public float GetY()
        {
            return this.y;
        }
        public float GetX()
        {
            return this.x;
        }
        
        public void SetX(float x)
        {
            this.x = x;
        }
        public void SetY(float y)
        {
            this.y = y;
        }
        public float GetWidthImage()
        {
            return Convert.ToSingle((float)this.image.Width);
        }
        public float GetHeightImage()
        {
            return Convert.ToSingle((float)this.image.Height);
        }
        public Rect GetRectangle()
        {
            return new Rect(this.x, this.y, this.image.Width, this.image.Height);
        }
        public Image GetImage()
        {
            return this.image;
        }
    }
}
